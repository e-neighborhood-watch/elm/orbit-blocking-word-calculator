module Main
  exposing
    ( ..
    )

import Array
  exposing
    ( Array
    )
import Browser
import Css
import Element
  exposing
    ( Element
    )
import Element.Background
  as Background
import Element.Border
  as Border
import Element.Font
  as Font
import Element.Input
  as Input
import Html
  as ElmHtml
  exposing
    ( Html
    )
import Html.Attributes
  as HtmlAttr
import Html.Styled
  as Html
import Svg.Styled
  as Svg
  exposing
    ( Svg
    )
import Svg.Styled.Attributes
  as SvgAttr

import Html.Math
  as Math
import Random
import Word
  exposing
    ( Word
    )
import Word.Automorphism
  as Automorphism
  exposing
    ( Automorphism
    )
import Word.Random
  as Random


type Message
 = OriginalWordChanged String
 | OrbitBlockingWordChanged String
 | AddAutomorphism Automorphism
 | ChangeNecklaceType Necklace
 | ChangeInputType
 | GenerateRandomWord
 | ChangeRandomWordSize Int


type KnownGenerators
  = None
  | FirstGenerator Word.Generator Word.Letter
  | BothGenerators Word.Letter Bool

type alias KmpEntry =
  { generatorChange : KnownGenerators -> KnownGenerators
  , backtrackDistance : Int
  }


type UnifyResult
  = Failure
  | NoChange
  | Update KnownGenerators


type Necklace
  = BeadNecklace
  | NumberNecklace Word.Generator


type alias Model =
  { originalWord : Word
  , inputType : Maybe Word.ParseType
  , originalWordString : String
  , currentWord : Word
  , potentialOrbitBlocker : Word
  , orbitBlockerString : String
  , automorphisms : List (Automorphism, Word)
  , kmpTable : Array KmpEntry
  , necklaceType : Necklace
  , randomWordSize : Int
  }


init : a -> ( Model, Cmd Message )
init _ =
  ( { originalWord =
      []
    , inputType =
      Nothing
    , originalWordString =
      ""
    , currentWord =
      []
    , potentialOrbitBlocker =
      []
    , orbitBlockerString =
      ""
    , automorphisms =
      []
    , kmpTable =
      Array.empty
    , necklaceType =
      BeadNecklace
    , randomWordSize =
      16
    }
  , Cmd.none
  )


wrapMathRow : List (Html a) -> Element a
wrapMathRow =
  Math.mrow []
    >> List.singleton
    >> Math.math []
    >> Element.html


viewAutomorphism : Bool -> Automorphism -> List (Html a)
viewAutomorphism positive auto =
  let
    letter : Word.Letter
    letter =
      { positive = positive
      , generator = auto.generator
      }

    attributesFunction : Word.Letter -> List (ElmHtml.Attribute a)
    attributesFunction curLetter =
      ["#" ++ letterColor curLetter |> HtmlAttr.style "color"]
  in
    Math.mapsTo
      [Math.generator attributesFunction letter]
      (List.map (Math.generator attributesFunction) (Automorphism.apply auto [letter]))


automorphismButton : Automorphism -> Element Message
automorphismButton auto =
  Input.button
    [ Element.paddingXY 20 10
    ]
    { onPress =
      Just (AddAutomorphism auto)
    , label =
      Element.textColumn
        [ Element.width Element.shrink
        ]
        [ Element.paragraph
          []
          [ viewAutomorphism True auto
            |> wrapMathRow
          ]
        , Element.paragraph
          []
          [ viewAutomorphism False auto
            |> wrapMathRow
          ]
        ]
    }


automorphismRow : Automorphism -> Element a
automorphismRow auto =
  Element.paragraph
    [ Element.width Element.fill
    ]
    [ viewAutomorphism True auto ++ Math.mo [] ";" :: viewAutomorphism False auto |> wrapMathRow
    ]


letterColor : Word.Letter -> String
letterColor letter =
  case
    letter.generator
  of
    Word.A ->
      if
        letter.positive
      then
        "FF5050"
      else
        "FF5050"
        -- "00AF00"

    Word.B ->
      if
        letter.positive
      then
        "0080FF"
      else
        "0080FF"
        -- "FF7F00"


type alias PointData a =
  { x : Float
  , y : Float
  , r : Float
  , datum : a
  }


polygon : (PointData b -> Svg a) -> List b -> List (Svg a)
polygon pointFunction data =
  let
    necklaceSize : Int
    necklaceSize =
      List.length data
        |> max 2

    angleStep : Float
    angleStep =
      2*pi / toFloat necklaceSize

    radius : Float
    radius =
      sin (angleStep / 2) / 2
        |> min 0.5

    makePoint : Int -> b -> Svg a
    makePoint ix datum =
      pointFunction
        { x =
          pi / 2 - angleStep * toFloat ix |> cos
        , y =
          pi / 2 - angleStep * toFloat ix |> sin
        , r =
          radius
        , datum =
          datum
        }
  in
    List.indexedMap makePoint data


viewBeadNecklace : Word -> List (Svg a)
viewBeadNecklace =
  let
    viewBead : PointData Word.Letter -> Svg a
    viewBead { x, y, r, datum } =
      case
        datum.generator
      of
        Word.A ->
          Svg.circle
            [ x |> String.fromFloat |> SvgAttr.cx
            , y |> String.fromFloat |> SvgAttr.cy
            , r |> String.fromFloat |> SvgAttr.r
            , SvgAttr.css
              [ Css.property "stroke-width" (String.fromFloat (0.1*r) ++ "px")
              , Css.property "stroke" ("#" ++ letterColor datum)
              , if
                  datum.positive
                then
                  letterColor datum |> Css.hex |> Css.fill
                else
                  Css.fill Css.transparent
              ]
            ]
            []

        Word.B ->
          Svg.rect
            [ x - r |> String.fromFloat |> SvgAttr.x
            , y - r |> String.fromFloat |> SvgAttr.y
            , 2*r |> String.fromFloat |> SvgAttr.width
            , 2*r |> String.fromFloat |> SvgAttr.height
            , SvgAttr.css
              [ Css.property "stroke-width" (String.fromFloat (0.1*r) ++ "px")
              , Css.property "stroke" ("#" ++ letterColor datum)
              , if
                  datum.positive
                then
                  letterColor datum |> Css.hex |> Css.fill
                else
                  Css.fill Css.transparent
              ]
            ]
            []
  in
    polygon viewBead


split : (a -> Bool) -> List a -> (List a, List a)
split pred =
  let
    go : List a -> List a -> (List a, List a)
    go lead end =
      case
        end
      of
        [] ->
          ( List.reverse lead, [] )

        head :: tail ->
          if
            pred head
          then
            go (head :: lead) tail
          else
            ( List.reverse lead, end )
  in
    go []


numberNecklace : Word.Generator -> Word -> List (Int, Bool)
numberNecklace gen word =
  let
    go : Word.Letter -> Word.Letter -> Int -> Word -> Word -> List (Int,Bool) -> List (Int,Bool)
    go initialOffLetter prevOffLetter numOn remaining end necklace =
      case
        remaining
      of
        [] ->
          case
            end
          of
            [] ->
              if
                initialOffLetter.positive
              then
                (numOn, not prevOffLetter.positive) :: List.reverse necklace
              else
                (negate numOn, prevOffLetter.positive) :: List.reverse necklace

            _ :: _ ->
              go initialOffLetter prevOffLetter numOn end [] necklace

        letter :: rest ->
          if
            letter.generator == gen
          then
            if
              letter.positive
            then
              go initialOffLetter prevOffLetter (numOn + 1) rest end necklace
            else
              go initialOffLetter prevOffLetter (numOn - 1) rest end necklace
          else
            if
              letter.positive
            then
              go initialOffLetter letter 0 rest end ((numOn, not prevOffLetter.positive) :: necklace)
            else
              go initialOffLetter letter 0 rest end ((negate numOn, prevOffLetter.positive) :: necklace)

    splitWord : (Word, Word)
    splitWord =
      split ( .generator >> (==) gen ) word
  in
    case
      Tuple.second splitWord
    of
      [] ->
        case
          Tuple.first splitWord
        of
          [] ->
            []

          _ :: _ ->
            [(0,True), (List.length (Tuple.first splitWord), True)]

      first :: rest ->
        go first first 0 rest (Tuple.first splitWord) []


viewNumberNecklace : Word.Generator -> Word -> List (Svg a)
viewNumberNecklace gen word =
  let
    viewNumber : PointData (Int, Bool) -> Svg a
    viewNumber { x, y, r, datum } =
      Svg.text_
        [ x |> String.fromFloat |> SvgAttr.x
        , y |> String.fromFloat |> SvgAttr.y
        , SvgAttr.css
          [ Css.property "dominant-baseline" "middle"
          , Css.property "text-anchor" "middle"
          , Css.fontSize (Css.px (2*r))
          , Css.fill (Word.Letter True gen |> letterColor |> Css.hex)
          ]
        ]
        [ Svg.text <|
          if
            Tuple.second datum
          then
            String.fromInt (Tuple.first datum) ++ "*"
          else
            String.fromInt (Tuple.first datum)
        ]
  in
    numberNecklace gen word
      |> polygon viewNumber
    


viewBody : Model -> Element Message
viewBody { originalWord, inputType, originalWordString, currentWord, potentialOrbitBlocker, orbitBlockerString, automorphisms, necklaceType, randomWordSize } =
  Element.column
    [ Element.centerX
    , Element.width Element.fill
    , Element.height Element.fill
    ]
    [ Svg.svg
      [ SvgAttr.width "280"
      , SvgAttr.height "120"
      , SvgAttr.viewBox "-3.6 -1.6 7.2 3.2"
      ]
      [ Svg.g
        [ SvgAttr.transform "translate(-2 0)"
        ]
        <|
          case
            necklaceType
          of
            BeadNecklace ->
              viewBeadNecklace originalWord

            NumberNecklace gen ->
              viewNumberNecklace gen originalWord
      , Svg.text_
        [ SvgAttr.x "0"
        , SvgAttr.y "0"
        , SvgAttr.css
          [ Css.property "dominant-baseline" "middle"
          , Css.property "text-anchor" "middle"
          , Css.fontSize (Css.px 0.9)
          ]
        ]
        [ Svg.text "↦"
        ]
      , Svg.g
        [ SvgAttr.transform "translate(2 0)"
        ]
        <|
          case
            necklaceType
          of
            BeadNecklace ->
              viewBeadNecklace currentWord

            NumberNecklace gen ->
              viewNumberNecklace gen currentWord
      ]
      |> Html.toUnstyled
      |> Element.html
      |>
        Element.el
          [ Element.centerX
          ]
    , Element.textColumn
      [ Font.center
      , Element.centerX
      ]
      [ Element.paragraph
        [ Font.size 30
        ]
        [ Math.mapsTo
          (List.map (always [] |> Math.generator) originalWord)
          (List.map (always [] |> Math.generator)  currentWord)
          |> wrapMathRow
        ]
      , Element.paragraph
        []
        [ potentialOrbitBlocker
          |> List.map (always [] |> Math.generator)
          |> wrapMathRow
        ]
      ]
    , Element.row
      [ Element.height Element.fill
      , Element.width Element.fill
      ]
      [ Element.column
        [ Font.size 16
        , Element.alignTop
        , Element.spacing 5
        ]
        [ Input.text
          [ Element.width (Element.px 160)
          ]
          { onChange =
            OriginalWordChanged
          , text =
            originalWordString
          , placeholder =
            Nothing
          , label =
            Element.text "Word:"
              |> Input.labelLeft []
          }
          {- , Input.text
          [ Element.width (Element.px 160)
          ]
          { onChange =
            OrbitBlockingWordChanged
          , text =
            orbitBlockerString
          , placeholder =
            Nothing
          , label =
            Element.text "Orbit blocking word:"
              |> Input.labelLeft []
          } -}
        , Input.button
          [ Background.color <|
            if
              inputType == Nothing
            then
              Element.rgb 0.5 0.5 0.5
            else
              Element.rgb 0.5 0.5 1
          , Border.rounded 10
          , Element.paddingXY 10 3
          , Font.color <|
            if
              inputType == Nothing
            then
              Element.rgb 0.2 0.2 0.2
            else
              Element.rgb 0.05 0.05 0.05
          ]
          { onPress =
            Maybe.map ( \ _ -> ChangeInputType ) inputType
          , label =
            case
              inputType
            of
              Nothing ->
                Element.text "Input empty"

              Just Word.Beads ->
                Element.text "Convert to blue number necklace"

              Just Word.NumberNecklace ->
                Element.text "Convert to word"
          }
        , Input.button
          [ Background.color <|
            Element.rgb 0.5 0.5 1
          , Border.rounded 10
          , Element.paddingXY 10 3
          , Font.color <|
            Element.rgb 0.05 0.05 0.05
          ]
          { onPress =
            Just GenerateRandomWord
          , label =
            Element.text "Generate Random Word"
          }
        , Input.slider
          []
          { onChange =
            round >> ChangeRandomWordSize
          , label =
            Input.labelAbove [] (Element.text ("Random Word Size: " ++ String.fromInt randomWordSize))
          , min =
            4
          , max =
            48
          , value =
            toFloat randomWordSize
          , thumb =
            Input.defaultThumb
          , step =
            Just 1
          }
        , Input.radio
          []
          { onChange =
            ChangeNecklaceType
          , selected =
            Just necklaceType
          , label =
            Input.labelAbove [] (Element.text "Necklace Type")
          , options =
            [ Input.option BeadNecklace (Element.text "Bead Necklace")
            , Input.option (NumberNecklace Word.A) (Element.text "Red Number Necklace")
            , Input.option (NumberNecklace Word.B) (Element.text "Blue Number Necklace")
            ]
          }
        ]
      , Element.column
        [ Element.width Element.shrink
        , Element.alignRight
        , Element.height Element.fill
        ]
        [ Element.column
          [ Element.width Element.fill
          , Element.height Element.fill
          , Element.scrollbarX
          ]
          (List.foldl (Tuple.first >> automorphismRow >> (::)) [] automorphisms)
        , Element.table
            [ Element.height Element.shrink
            , Font.center
            , Element.alignBottom
            ]
            { data =
              [ True, False ]
            , columns =
              let
                column : (Bool -> Automorphism) -> Element.Column Bool Message
                column preAuto =
                  { header =
                    Element.none
                  , width =
                    Element.shrink
                  , view =
                    preAuto
                      >> automorphismButton
                  }
              in
                List.map
                  column
                    [ Automorphism Word.A
                    , Automorphism Word.B
                    ]
            }
        ]
      ]
    ]


view : Model -> Browser.Document Message
view model =
  { title =
    "Orbit Blocking Words"
  , body =
    [ model
      |> viewBody
      |> Element.layout []
    ]
  }


unifyLetter : Word.Letter -> Word.Letter -> KnownGenerators -> UnifyResult
unifyLetter new target knownGenerators =
  case
    knownGenerators
  of
    None ->
      { generator =
        target.generator
      , positive =
        new.positive == target.positive
      }
      |> FirstGenerator new.generator
      |> Update

    FirstGenerator gen1 gen1Target ->
      if
        new.generator == gen1
      then
        if
          target.generator == gen1Target.generator
            && new.positive == (target.positive == gen1Target.positive)
        then
          NoChange
        else
          Failure
      else
        if
          target.generator == gen1Target.generator
        then
          Failure
        else
          Update <|
          case
            gen1
          of
            Word.A ->
              BothGenerators
                gen1Target
                (new.positive == target.positive)

            Word.B ->
              BothGenerators
                { generator =
                  target.generator
                , positive =
                  new.positive == target.positive
                }
                gen1Target.positive

    BothGenerators aTarget bTargetPositive ->
      case
        new.generator
      of
        Word.A ->
          if
            target.generator == aTarget.generator
              && new.positive == (target.positive == aTarget.positive)
          then
            NoChange
          else
            Failure

        Word.B ->
          if
            target.generator /= aTarget.generator
              && new.positive == (target.positive == bTargetPositive)
          then
            NoChange
          else
            Failure
          


findWord : Word -> Array Word.Letter -> Array KmpEntry -> Maybe Int
findWord =
  let
    go : Int -> Int -> KnownGenerators -> Word -> Array Word.Letter -> Array KmpEntry -> Maybe Int
    go foundIx toFindIx knownGenerators toSearch toFind table =
      case
        toSearch
      of
        [] ->
          Nothing

        next :: rest ->
          case
            Array.get toFindIx toFind
          of
            Nothing ->
              Just (foundIx - toFindIx)

            Just letter ->
              case
                unifyLetter letter next knownGenerators
              of
                Failure ->
                  -- only happens when toFindIx > 0
                  case
                    Array.get (toFindIx - 2) table
                  of
                    Nothing ->
                      -- only happens when toFindIx == 1
                      go foundIx 0 None toSearch toFind table

                    Just { backtrackDistance, generatorChange } ->
                      go foundIx backtrackDistance (generatorChange knownGenerators) toSearch toFind table

                Update newKnownGenerators ->
                  go (foundIx + 1) (toFindIx + 1) newKnownGenerators rest toFind table

                NoChange ->
                  go (foundIx + 1) (toFindIx + 1) knownGenerators rest toFind table
  in
    go 0 0 None
      


buildKmpTable : Word -> Maybe (Array KmpEntry)
buildKmpTable word =
  let
    wordArray : Array Word.Letter
    wordArray =
      Array.fromList word

    growKmpTable : Int -> Int -> KnownGenerators -> Array KmpEntry -> Array KmpEntry
    growKmpTable searchIndex prefixIndex knownGenerators table =
      case
        Array.get searchIndex wordArray
      of
        Nothing ->
          table

        Just next ->
          case
            Array.get prefixIndex wordArray
          of
            Nothing ->
              table -- should never happen

            Just letter ->
              case
                unifyLetter letter next knownGenerators
              of
                Failure ->
                  -- only happens when prefixIndex > 0
                  case
                    Array.get (prefixIndex - 2) table
                  of
                    Nothing ->
                      -- only happens when prefixIndex == 1
                      growKmpTable searchIndex 0 None table

                    Just { backtrackDistance, generatorChange } ->
                      growKmpTable searchIndex backtrackDistance (generatorChange knownGenerators) table

                NoChange ->
                  table -- TODO

                Update newKnownGenerators ->
                  -- TODO
                  Array.push
                    { backtrackDistance =
                      prefixIndex + 1
                    , generatorChange =
                      \ _ -> newKnownGenerators
                    }
                    table
                    |> growKmpTable (searchIndex + 1) (prefixIndex + 1) newKnownGenerators
  in
    case
      word
    of
      [] ->
        Nothing

      _ :: _->
        growKmpTable 2 0 None Array.empty
          |> Just

update : Message -> Model -> ( Model, Cmd Message )
update msg prev =
  case
    msg
  of
    OriginalWordChanged newWord ->
      case
        Word.parseBeadsOrNumberNecklace newWord
      of
        Nothing ->
          ( { prev
            | originalWordString =
              newWord
            }
          , Cmd.none
          )

        Just { parseType, word } ->
          let
            cycliclyReducedWord : Word
            cycliclyReducedWord =
              Word.cycliclyReduce word

            newAutomorphisms : (List (Automorphism, Word), Word)
            newAutomorphisms =
              List.foldr (\ (a,_) (b,c) -> ((a,c)::b,Automorphism.apply a c)) ([], cycliclyReducedWord) prev.automorphisms
          in
            ( { prev
              | originalWordString =
                newWord
              , originalWord =
                cycliclyReducedWord
              , automorphisms =
                Tuple.first newAutomorphisms
              , currentWord =
                Tuple.second newAutomorphisms
              , inputType =
                parseType
              }
            , Cmd.none
            )

    OrbitBlockingWordChanged newWord ->
      case
        Word.parse newWord
      of
        Nothing ->
          ( { prev
            | orbitBlockerString =
              newWord
            }
          , Cmd.none
          )

        Just parsedWord ->
          ( { prev
            | orbitBlockerString =
              newWord
            , potentialOrbitBlocker =
              parsedWord
            }
          , Cmd.none
          )

    AddAutomorphism auto ->
      case
        prev.automorphisms
      of
        [] ->
          ( { prev
            | currentWord =
              Automorphism.apply auto prev.currentWord
            , automorphisms =
              [(auto,prev.currentWord)]
            }
          , Cmd.none
          )

        (recent,lastWord) :: rest ->
          if
            auto.generator == recent.generator
              && auto.positive /= recent.positive
          then
            ( { prev
              | currentWord =
                lastWord
              , automorphisms =
                rest
              }
            , Cmd.none
            )
          else
            ( { prev
              | currentWord =
                Automorphism.apply auto prev.currentWord
              , automorphisms =
                (auto,prev.currentWord) :: prev.automorphisms
              }
            , Cmd.none
            )

    ChangeNecklaceType newType ->
      ( { prev
        | necklaceType =
          newType
        }
      , Cmd.none
      )

    ChangeInputType ->
      case
        prev.inputType
      of
        Nothing ->
          ( prev
          , Cmd.none
          )

        Just Word.NumberNecklace ->
          ( { prev
            | inputType =
              Just Word.Beads
            , originalWordString =
              Word.toString prev.originalWord
            }
          , Cmd.none
          )

        Just Word.Beads ->
          ( { prev
            | inputType =
              Just Word.NumberNecklace
            , originalWordString =
                let
                  beadToString : (Int, Bool) -> String
                  beadToString ( exp, star ) =
                    if
                      star
                    then
                      String.fromInt exp ++ "*"
                    else
                      String.fromInt exp
                in
                  numberNecklace Word.B prev.originalWord
                    |> List.map beadToString
                    |> String.join " "
            }
          , Cmd.none
          )

    GenerateRandomWord ->
      ( prev
      , Random.generate OriginalWordChanged (Random.wordString prev.randomWordSize)
      )

    ChangeRandomWordSize newSize ->
      ( { prev
        | randomWordSize =
          newSize
        }
      , Cmd.none
      )


subscriptions : Model -> Sub Message
subscriptions _ =
  Sub.none


main : Program {} Model Message
main =
  Browser.document
    { init =
      init
    , view =
      view
    , update =
      update
    , subscriptions =
      subscriptions
    }
