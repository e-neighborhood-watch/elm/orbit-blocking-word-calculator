module Html.Math
  exposing
    ( math
    , mrow
    , generator
    , mapsTo
    , mo
    )


import Word
  exposing
    ( Word
    )


import Html
  exposing
    ( Html
    , Attribute
    )
import Html.Attributes
  as HtmlAttr
import VirtualDom


node : String -> List (Attribute msg) -> List (Html msg) -> Html msg
node =
  VirtualDom.nodeNS "http://www.w3.org/1998/Math/MathML"

math : List (Attribute msg) -> List (Html msg) -> Html msg
math attrs =
  node "math" (HtmlAttr.attribute "display" "block" :: attrs)


m_ : String -> List (Attribute msg) -> String -> Html msg
m_ name =
  node name
    >> (>>) (Html.text >> List.singleton)


mo : List (Attribute msg) -> String -> Html msg
mo =
  m_ "mo"


mi : List (Attribute msg) -> String -> Html msg
mi =
  m_ "mi"


msup : List (Attribute msg) -> Html msg -> Html msg -> Html msg
msup attrs base exponent =
  node "msup" attrs
    [ base
    , exponent
    ]


msub : List (Attribute msg) -> Html msg -> Html msg -> Html msg
msub attrs base subscript =
  node "msub" attrs
    [ base
    , subscript
    ]


msubsup : List (Attribute msg) -> Html msg -> Html msg -> Html msg -> Html msg
msubsup attrs base subscript superscript =
  node "msubsup" attrs
    [ base
    , subscript
    , superscript
    ]


mn : List (Attribute msg) -> String -> Html msg
mn =
  m_ "mn"


mnInt : List (Attribute msg) -> Int -> Html msg
mnInt =
  mn >> (>>) String.fromInt


mnFloat : List (Attribute msg) -> Float -> Html msg
mnFloat =
  mn >> (>>) String.fromFloat


mrow : List (Attribute msg) -> List (Html msg) -> Html msg
mrow =
  node "mrow"


generator : (Word.Letter -> List (Attribute msg)) -> Word.Letter -> Html msg
generator attrs letter =
  case
    letter.generator
  of
    Word.A ->
      if
        letter.positive
      then
        mi (attrs letter) "a"
      else
        msup (attrs letter)
          (mi [] "a")
          (mnInt [] -1)

    Word.B ->
      if
        letter.positive
      then
        mi (attrs letter) "b"
      else
        msup (attrs letter)
          (mi [] "b")
          (mnInt [] -1)


mapsTo : List (Html a) -> List (Html a) -> List (Html a)
mapsTo from to =
  from ++ mo [] "↦" :: to

