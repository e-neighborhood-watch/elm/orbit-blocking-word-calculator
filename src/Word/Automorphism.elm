module Word.Automorphism
  exposing
    ( Automorphism
    , apply
    )


import Word
  exposing
    ( Generator(..)
    , Word
    , Letter
    )


type alias Automorphism =
  { generator : Generator
  , positive : Bool
  }


otherGenerator : Generator -> Generator
otherGenerator gen =
  case
    gen
  of
    A ->
      B

    B ->
      A


apply : Automorphism -> Word -> Word
apply { generator, positive } =
  let
    positiveImage : Word
    positiveImage =
      [ Letter positive (otherGenerator generator)
      , Letter True generator
      ]

    negativeImage : Word
    negativeImage =
      Word.invert positiveImage

    applyToLetter : Letter -> Word
    applyToLetter l =
      if
        generator == l.generator
      then
        if
          l.positive
        then
          positiveImage
        else
          negativeImage
      else
        [ l ]
  in
    List.concatMap applyToLetter
      >> Word.reduce
      >> Word.cycliclyReduce
