module Word.Random
  exposing
    ( wordString
    )


import Random
  exposing
    ( Generator
    )


oneOf : Generator a -> List (Generator a) -> Generator a
oneOf x =
  Random.uniform x
    >> Random.andThen identity


beadToString : Int -> Bool -> String
beadToString exp star =
  if
    star
  then
    " " ++ String.fromInt exp ++ "*"
  else
    " " ++ String.fromInt exp


wordString : Int -> Generator String
wordString k =
  let
    go : Bool -> Int -> String -> Int -> Generator String
    go needsStar currentNum necklace n =
      if
        n == 1
      then
        if
          needsStar && currentNum == 0
        then
          Random.uniform
            ( necklace ++ " 1*" )
            [ necklace ++ " -1*"
            ]
        else
          oneOf
            ( if
                String.isEmpty necklace
              then
                oneOf
                  ( String.fromInt (currentNum + 1) ++ "* 0*"
                      |> Random.constant
                  )
                  [ Random.uniform
                    ( necklace ++ " 0" ++ beadToString currentNum needsStar )
                    [ necklace ++ " 0" ++ beadToString (negate currentNum) needsStar
                    ]
                  ] 
              else
                Random.uniform
                  ( necklace ++ " 0" ++ beadToString currentNum needsStar )
                  [ necklace ++ " 0" ++ beadToString (negate currentNum) needsStar
                  ]
            )
            [ Random.uniform
              ( necklace ++ beadToString (currentNum + 1) needsStar )
              [ necklace ++ beadToString (negate currentNum - 1) needsStar
              ]
            ]
      else
        if
          currentNum == 0
        then
          oneOf
            ( Random.lazy ( \ _ -> go needsStar 1 necklace (n - 1) ) )
            [ Random.lazy ( \ _ -> go needsStar 0 (necklace ++ " 0") (n - 1) )
            ]
        else
          oneOf
            ( Random.lazy ( \ _ -> go needsStar (currentNum + 1) necklace (n - 1) ) )
            [ oneOf
                ( Random.lazy ( \ _ -> go needsStar 0 (necklace ++ " " ++ String.fromInt currentNum) (n - 1) ) )
                [ Random.lazy ( \ _ -> go needsStar 0 (necklace ++ " " ++ String.fromInt (negate currentNum)) (n - 1) )
                , Random.lazy ( \ _ -> go (not needsStar) 0 (necklace ++ " " ++ String.fromInt currentNum ++ "*") (n - 1) )
                , Random.lazy ( \ _ -> go (not needsStar) 0 (necklace ++ " " ++ String.fromInt (negate currentNum) ++ "*") (n - 1) )
                ]
            ]
  in
    case
      compare k 1
    of
      LT ->
        Random.constant ""

      EQ ->
        oneOf
          ( Random.constant
            "0"
          )
          [ Random.uniform
            "1* 0*"
            [ "-1* 0*"
            ]
          ]

      GT ->
        go False 0 "" (k - 1)
          |> Random.map String.trim
