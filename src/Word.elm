module Word
  exposing
    ( Word
    , Letter
    , Generator(..)
    , ParseType(..)
    , ParseResult
    , parse
    , parseNumberNecklace
    , parseBeadsOrNumberNecklace
    , reduce
    , cycliclyReduce
    , invert
    , toString
    )


import Parser
  exposing
    ( Parser
    , (|.)
    , (|=)
    )


type ParseType
  = Beads
  | NumberNecklace


type Generator
  = A
  | B


type alias Letter =
  { positive : Bool
  , generator : Generator
  }


type alias Word =
  List Letter


type ItemVariety
  = Simple Generator
  | Commutator Word Word
  | Parenthetical Word


type alias Item =
  { variety : ItemVariety
  , exponent : Int
  }


type alias Bead =
  { exponent : Int
  , starred : Bool
  }


toString : Word -> String
toString w =
  case
    w
  of
    [] ->
      ""

    head :: rest ->
      let
        buildSyllable : Letter -> ((Generator, Int), String) -> ((Generator, Int), String)
        buildSyllable { positive, generator } ((prevGen, exponent), str) =
          let
            generatorSign : Int
            generatorSign =
              if
                positive
              then
                1
              else
                -1
          in
            ( ( generator
              , if
                  prevGen /= generator
                then
                  generatorSign
                else
                  exponent + generatorSign
              )
            , if
                prevGen /= generator
              then
                case
                  prevGen
                of
                  A ->
                    str ++ "a^" ++ String.fromInt exponent

                  B ->
                    str ++ "b^" ++ String.fromInt exponent
              else
                str
            )

        ( ( lastGenerator, lastExponent ), almostStr ) =
          List.foldl
            buildSyllable
            ( ( head.generator
              , if
                  head.positive
                then
                  1
                else
                  -1
              )
            , ""
            )
            rest
      in
        case
          lastGenerator
        of
          A ->
            almostStr ++ "a^" ++ String.fromInt lastExponent

          B ->
            almostStr ++ "b^" ++ String.fromInt lastExponent


mkItem : (ItemVariety, Int) -> Int -> Item
mkItem (var,inv) power =
  { variety =
    var
  , exponent =
    inv*power
  }


inverses : Letter -> Letter -> Bool
inverses l1 l2 =
  l1.generator == l2.generator && l1.positive /= l2.positive


invert : Word -> Word
invert =
  List.foldl
    ( \ letter -> (::) { letter | positive = not letter.positive } )
    []


reduce : Word -> Word
reduce =
  let
    addLetter : Letter -> Word -> Word
    addLetter l w =
      case
        w
      of
        [] ->
          [l]

        l2 :: rest ->
          if
            inverses l l2
          then
            rest
          else
            l :: w
  in
    List.foldr addLetter []


cycliclyReduce : Word -> Word
cycliclyReduce word =
  let
    removePrefix : Word -> Word -> Int -> (Word, Int)
    removePrefix forward reverse removed =
      case
        forward
      of
        [] ->
          ( [], removed )

        fl :: frest ->
          case
            reverse
          of
            [] ->
              ( [], removed ) -- should never happen

            rl :: rrest ->
              if
                inverses fl rl
              then
                removePrefix frest rrest (removed+1)
              else
                ( forward, removed )

    ( withoutPrefix, removedNum ) =
      removePrefix word (List.reverse word) 0
  in
    List.take (List.length withoutPrefix - removedNum) withoutPrefix
    


itemToWord : Item -> Word
itemToWord { variety, exponent } =
  case
    variety
  of
    Simple gen ->
      if
        exponent >= 0
      then 
        List.repeat exponent (Letter True gen)
      else
        List.repeat (negate exponent) (Letter False gen)

    _ ->
      []


parse : String -> Maybe Word
parse =
  Parser.run (parser |. Parser.end)
    -- >> Result.mapError (Debug.log "Parser error")
    >> Result.toMaybe


intWithNegative : Parser Int
intWithNegative =
  Parser.oneOf
    [ Parser.succeed negate
      |. Parser.symbol "-"
      |= Parser.int
    , Parser.int
    ]


item : Parser Item
item =
  Parser.succeed mkItem
    |=
      Parser.oneOf
        [ Parser.map ( \ _ -> (Simple A, 1) ) (Parser.symbol "a")
        , Parser.map ( \ _ -> (Simple A, -1) ) (Parser.symbol "A")
        , Parser.map ( \ _ -> (Simple B, 1) ) (Parser.symbol "b")
        , Parser.map ( \ _ -> (Simple B, -1) ) (Parser.symbol "B")
        ]
    |=
      Parser.oneOf
        [ Parser.succeed identity
          |. Parser.symbol "^"
          |= intWithNegative
        , Parser.succeed 1
        ]


parser : Parser Word
parser =
  Parser.sequence
    { start =
      ""
    , separator =
      ""
    , end =
      ""
    , spaces =
      Parser.spaces
    , item =
      item
    , trailing =
      Parser.Optional
    }
    |> Parser.map (List.concatMap itemToWord >> reduce)


parseNumberNecklace : String -> Maybe Word
parseNumberNecklace =
  Parser.run (numberNecklaceParser |. Parser.end)
    -- >> Result.mapError (Debug.log "Parser error")
    >> Result.toMaybe


bead : Parser Bead
bead =
  Parser.succeed Bead
    |= intWithNegative
    |=
      Parser.oneOf
        [ Parser.map ( \ _ -> True ) (Parser.symbol "*")
        , Parser.succeed False
        ]


-- a +  => a b... a
-- a -  => b B... a
-- A +  => A B... A
-- A -  => A b... A
-- a +* => a B... A
-- a -* => a b... A
-- A +* => A b... a
-- A -* => A B... a
buildNecklace : Bool -> Word -> Int -> Bool -> Parser.Step (Bool, Word) a
buildNecklace positive prevWord exponent starred =
  let
    wordBead : Word
    wordBead =
      { positive =
        ((exponent > 0) == positive) /= starred
      , generator =
        B
      }
        |> List.repeat (abs exponent)
  in
    Parser.Loop
      ( starred /= positive
      , { positive =
          positive
        , generator =
          A
        }
          :: prevWord
          |> (++) wordBead
      )


parseBead : ( Bool, Word ) -> Parser (Parser.Step ( Bool, Word ) Word)
parseBead (positive, prevWord) =
  Parser.succeed identity
    |. Parser.spaces
    |=
      Parser.oneOf
        [ Parser.succeed (buildNecklace positive prevWord)
          |= intWithNegative
          |=
            Parser.oneOf
              [ Parser.map ( \ _ -> True ) (Parser.symbol "*")
              , Parser.succeed False
              ]
        , if
            positive
          then
            prevWord
              |> List.reverse
              |> reduce
              |> Parser.Done
              |> Parser.succeed
          else
            Parser.problem "Odd number of stars"
        ]


numberNecklaceParser : Parser Word
numberNecklaceParser =
  Parser.loop (True, []) parseBead


type alias ParseResult =
  { parseType : Maybe ParseType
  , word : Word
  }


parseBeadsOrNumberNecklace : String -> Maybe ParseResult
parseBeadsOrNumberNecklace =
  Parser.run
    ( Parser.oneOf
      [ Parser.succeed (ParseResult Nothing []) |. Parser.spaces |. Parser.end |> Parser.backtrackable
      , numberNecklaceParser |. Parser.end |> Parser.backtrackable |> Parser.map (ParseResult (Just NumberNecklace))
      , parser |. Parser.end |> Parser.map (ParseResult (Just Beads))
      ]
    )
    -- >> Result.mapError (Debug.log "Parser error")
    >> Result.toMaybe
